var url = ajaxurl;
var controller = 'Ssl';
var option = "com_ose_firewall";
var settings = new Array();
var domain = null;
var base_url = null;
var download_domain = null;
var download_issueddate = null;

jQuery(document).ready(function($){
    $("#sslCertTable").hide();
    $("#ssl_download_btn").click(function(){
        $("#sslCertTable").toggle();
    });

    checkDomainStatus();
    getBaseUrl();

    var certificatesDataTable = $('#certificatesTable').dataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: url,
            type: "POST",
            data:  {
                option : option,
                controller : controller,
                action : 'getCertificateList',
                task : 'getCertificateList',
                centnounce : $('#centnounce').val(),
            }
        },
        columns: [
            {"data": "id"},
            {"data": "ssl_domain"},
            {"data": "issued_date"},
            {"data": "download" , sortable: false },
        ]
    });
    $('#certificatesTable tbody').on('click', 'tr', function () {
        $(this).toggleClass('selected');
    });

    //var statusFilter = $('<label>Status: <select name="statusFilter" id="statusFilter">' +
    //    '<option value="4">All</option>' +
    //    '<option value="1">WhiteListed</option>' +
    //    '<option value="2">BlackListed</option>' +
    //    '<option value="3">Monitored</option>' +
    //    '</select></label>');
    //statusFilter.appendTo($("#certificatesTable_filter")).on('change', function () {
    //    var val = $('#statusFilter');
    //    manageIPsDataTable.api().column(2)
    //        .search(val.val(), false, false)
    //        .draw();
    //});
});
angular
    .module('angularStates', ['ui.router', 'ngAnimate'])
    .config(function($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise("/home");
        $stateProvider
            .state('home', {
                url: "/home",
                views: {
                    'input-views': {
                        template: '',
                        controller: function($rootScope) {
                            $rootScope.homePageIsShown = true;
                        }
                    }
                }
            })
            //for the domain validation
            .state('state1', {
                url: "/state1",
                views: {
                    'input-views': {
                        template: '<div class="inner-wrapper">' +
                        '<div class="slide-header">Choose Domain</div>' +
                        '<div class="slide-desc">The domain your want to get SSL</div>' +
                        '<div class="button-wrapper">' +
                        '<div id="select_ssl_domain">' +
                            //'<div id="switch-ssl-domain">' +
                        //'<div class="diamond"></div>' +
                        //'<div class="choice select_ssl_domain on ">Select Domain</div>' +
                        //'<div class="choice new_ssl_domain">New Domain</div>' +
                        '</div>' +
                        //'<select id="select_ssl_domain" class="form-control" >' +
                        //'</select>' +
                        //'<input id="select_ssl_path" class="form-control" placeholder="Please Enter The Path">' +
                        //'<input id="new_ssl_domain" class="form-control" placeholder="Please Enter The NEW Domain">' +
                        //'<input id="new_ssl_path" class="form-control" placeholder="Please Enter The Path">' +
                        '<a href="#" ui-sref="home">Cancel</a>' +
                        '<a href="#" ui-sref="state1" onclick = "queryEmailForValidation(0)">Continue</a>' +
                        '</div>' +
                        '</div>',
                        controller: function($rootScope) {
                            getDomain();
                            $rootScope.homePageIsShown = false;
                            checkAccess('state1');
                        }
                    }
                }
            })
            //for domain revalidation
            .state('state1a', {
                url: "/state1a",
                views: {
                    'input-views': {
                        template: '<div class="inner-wrapper">' +
                        '<div class="slide-header">Choose Domain</div>' +
                        '<div class="slide-desc">The domain your want to get SSL</div>' +
                        '<div class="button-wrapper">' +
                        '<div id="select_ssl_domain">' +
                        '</div>' +
                        '<a href="#" ui-sref="home">Cancel</a>' +
                        '<a href="#" ui-sref="state1a" onclick = "queryEmailForValidation(1)">Continue</a>' +
                        '</div>' +
                        '</div>',
                        controller: function($rootScope) {
                            getDomain();
                            $rootScope.homePageIsShown = false;
                            checkAccess('state1');
                        }
                    }
                }
            })
            .state('state2', {
                url: "/state2",
                data: {
                    prev: 'state1'
                },
                views: {
                    'input-views': {
                        template: '<div class="inner-wrapper">' +
                        '<div class="slide-header">Choose Email</div>' +
                        '<div class="slide-desc">The domain email will receive verification code</div>' +
                        '<div class="button-wrapper">' +
                        '<select id="select_ssl_email" class="form-control" >' +
                        '</select>' +
                        '<a href="#" ui-sref="state1">Previous</a>' +
                        '<a id = "send_verification_button"  href="#" ui-sref="state3" onclick="sendVerificationCode()">Send Verification Code</a>' +
                        '</div>' +
                        '</div>',
                        controller: function($rootScope) {
                            $rootScope.homePageIsShown = false;
                            checkAccess('state2');
                        }
                    }
                }
            })
            .state('state2a', {
                url: "/state2a",
                data: {
                    prev: 'state1a'
                },
                views: {
                    'input-views': {
                        template: '<div class="inner-wrapper">' +
                        '<div class="slide-header">Choose Email</div>' +
                        '<div class="slide-desc">The Re validation code will be sent to the chosen email address</div>' +
                        '<div class="button-wrapper">' +
                        '<select id="select_ssl_email" class="form-control" >' +
                        '</select>' +
                        '<a href="#" ui-sref="state1a">Previous</a>' +
                        '<a id = "send_verification_button"  href="#" ui-sref="state3a" onclick="sendReVerificationCode()">Send Verification Code</a>' +
                        '</div>' +
                        '</div>',
                        controller: function($rootScope) {
                            $rootScope.homePageIsShown = false;
                            checkAccess('state2');
                        }
                    }
                }
            })


            .state('state3', {
                url: "/state3",
                data: {
                    prev: 'state2'
                },
                views: {
                    'input-views': {
                        template: '<div class="inner-wrapper">' +
                        '<div class="slide-header">Validation</div>' +
                        '<div class="slide-desc">Verifiy your code</div>' +
                        '<div class="button-wrapper">' +
                        '<input id="input_ssl_validation" class="form-control" placeholder="Please Enter Your Verification Code">' +
                        '<a href="#" ui-sref="state2" id = "verification_code_bbutton">Previous</a>' +
                        '<a href="#" ui-sref="state3" id = "verification_code_nbutton" onclick="validateDomain()">Verify</a>' +
                        '</div>' +
                        '</div>',
                        controller: function($rootScope) {
                            $rootScope.homePageIsShown = false;
                            checkAccess('state3');
                        }
                    }
                }
            })

            .state('state3a', {
                url: "/state3a",
                data: {
                    prev: 'state2a'
                },
                views: {
                    'input-views': {
                        template: '<div class="inner-wrapper">' +
                        '<div class="slide-header">Re - Validation</div>' +
                        '<div class="slide-desc">Verifiy your code to Re Validate the domain</div>' +
                        '<div class="button-wrapper">' +
                        '<input id="input_ssl_validation" class="form-control" placeholder="Please Enter Your Verification Code">' +
                        '<a href="#" ui-sref="state2a" id = "verification_code_bbutton">Previous</a>' +
                        '<a href="#" ui-sref="state3a" id = "verification_code_nbutton" onclick="reValidateDomain()">Re validate</a>' +
                        '</div>' +
                        '</div>',
                        controller: function($rootScope) {
                            $rootScope.homePageIsShown = false;
                            checkAccess('state3');
                        }
                    }
                }
            })

            .state('state4', {
                url: "/state4",
                data: {
                    prev: 'state3'
                },
                views: {
                    'input-views': {
                        template: '<div class="inner-wrapper">' +
                        '<div class="slide-header">Generate SSL Certificate</div>' +
                        '<div class="button-wrapper">' +
                        '<select id="input_ssl_country" class="form-control generateSSL" placeholder="Country Name">' +
                        '<option value="">Country Name</option>' +
                        '<option label="Afghanistan" value="AF">Afghanistan</option>' +
                        '<option label="Åland Islands" value="AX">Åland Islands</option>' +
                        '<option label="Albania" value="AL">Albania</option>' +
                        '<option label="Algeria" value="DZ">Algeria</option>' +
                        '<option label="American Samoa" value="AS">American Samoa</option>' +
                        '<option label="Andorra" value="AD">Andorra</option>' +
                        '<option label="Angola" value="AO">Angola</option>' +
                        '<option label="Anguilla" value="AI">Anguilla</option>' +
                        '<option label="Antarctica" value="AQ">Antarctica</option>' +
                        '<option label="Antigua and Barbuda" value="AG">Antigua and Barbuda</option>' +
                        '<option label="Argentina" value="AR">Argentina</option>' +
                        '<option label="Armenia" value="AM">Armenia</option>' +
                        '<option label="Aruba" value="AW">Aruba</option>' +
                        '<option label="Australia" value="AU">Australia</option>' +
                        '<option label="Austria" value="AT">Austria</option>' +
                        '<option label="Azerbaijan" value="AZ">Azerbaijan</option>' +
                        '<option label="Bahamas" value="BS">Bahamas</option>' +
                        '<option label="Bahrain" value="BH">Bahrain</option>' +
                        '<option label="Bangladesh" value="BD">Bangladesh</option>' +
                        '<option label="Barbados" value="BB">Barbados</option>' +
                        '<option label="Belarus" value="BY">Belarus</option>' +
                        '<option label="Belgium" value="BE">Belgium</option>' +
                        '<option label="Belize" value="BZ">Belize</option>' +
                        '<option label="Benin" value="BJ">Benin</option>' +
                        '<option label="Bermuda" value="BM">Bermuda</option>' +
                        '<option label="Bhutan" value="BT">Bhutan</option>' +
                        '<option label="Bolivia, Plurinational State of" value="BO">Bolivia, Plurinational State of</option>' +
                        '<option label="Bonaire, Sint Eustatius and Saba" value="BQ">Bonaire, Sint Eustatius and Saba</option>' +
                        '<option label="Bosnia and Herzegovina" value="BA">Bosnia and Herzegovina</option>' +
                        '<option label="Botswana" value="BW">Botswana</option>' +
                        '<option label="Bouvet Island" value="BV">Bouvet Island</option>' +
                        '<option label="Brazil" value="BR">Brazil</option>' +
                        '<option label="British Indian Ocean Territory" value="IO">British Indian Ocean Territory</option>' +
                        '<option label="Brunei Darussalam" value="BN">Brunei Darussalam</option>' +
                        '<option label="Bulgaria" value="BG">Bulgaria</option>' +
                        '<option label="Burkina Faso" value="BF">Burkina Faso</option>' +
                        '<option label="Burundi" value="BI">Burundi</option>' +
                        '<option label="Cambodia" value="KH">Cambodia</option>' +
                        '<option label="Cameroon" value="CM">Cameroon</option>' +
                        '<option label="Canada" value="CA">Canada</option>' +
                        '<option label="Cape Verde" value="CV">Cape Verde</option>' +
                        '<option label="Cayman Islands" value="KY">Cayman Islands</option>' +
                        '<option label="Central African Republic" value="CF">Central African Republic</option>' +
                        '<option label="Chad" value="TD">Chad</option>' +
                        '<option label="Chile" value="CL">Chile</option>' +
                        '<option label="China" value="CN">China</option>' +
                        '<option label="Christmas Island" value="CX">Christmas Island</option>' +
                        '<option label="Cocos (Keeling) Islands" value="CC">Cocos (Keeling) Islands</option>' +
                        '<option label="Colombia" value="CO">Colombia</option>' +
                        '<option label="Comoros" value="KM">Comoros</option>' +
                        '<option label="Congo" value="CG">Congo</option>' +
                        '<option label="Congo, the Democratic Republic of the" value="CD">Congo, the Democratic Republic of the</option>' +
                        '<option label="Cook Islands" value="CK">Cook Islands</option>' +
                        '<option label="Costa Rica" value="CR">Costa Rica</option>' +
                        '<option label="Cote D Ivoire" value="CI">Cote D Ivoire</option>' +
                        '<option label="Croatia" value="HR">Croatia</option>' +
                        '<option label="Cuba" value="CU">Cuba</option>' +
                        '<option label="Curaçao" value="CW">Curaçao</option>' +
                        '<option label="Cyprus" value="CY">Cyprus</option>' +
                        '<option label="Czech Republic" value="CZ">Czech Republic</option>' +
                        '<option label="Denmark" value="DK">Denmark</option>' +
                        '<option label="Djibouti" value="DJ">Djibouti</option>' +
                        '<option label="Dominica" value="DM">Dominica</option>' +
                        '<option label="Dominican Republic" value="DO">Dominican Republic</option>' +
                        '<option label="East Timor" value="TP">East Timor</option>' +
                        '<option label="Ecuador" value="EC">Ecuador</option>' +
                        '<option label="Egypt" value="EG">Egypt</option>' +
                        '<option label="El Salvador" value="SV">El Salvador</option>' +
                        '<option label="Equatorial Guinea" value="GQ">Equatorial Guinea</option>' +
                        '<option label="Eritrea" value="ER">Eritrea</option>' +
                        '<option label="Estonia" value="EE">Estonia</option>' +
                        '<option label="Ethiopia" value="ET">Ethiopia</option>' +
                        '<option label="Falkland Islands (Malvinas)" value="FK">Falkland Islands (Malvinas)</option>' +
                        '<option label="Faroe Islands" value="FO">Faroe Islands</option>' +
                        '<option label="Fiji" value="FJ">Fiji</option>' +
                        '<option label="Finland" value="FI">Finland</option>' +
                        '<option label="France" value="FR">France</option>' +
                        '<option label="French Guiana" value="GF">French Guiana</option>' +
                        '<option label="French Polynesia" value="PF">French Polynesia</option>' +
                        '<option label="French Southern Territories" value="TF">French Southern Territories</option>' +
                        '<option label="Gabon" value="GA">Gabon</option>' +
                        '<option label="Gambia" value="GM">Gambia</option>' +
                        '<option label="Georgia" value="GE">Georgia</option>' +
                        '<option label="Germany" value="DE">Germany</option>' +
                        '<option label="Ghana" value="GH">Ghana</option>' +
                        '<option label="Gibraltar" value="GI">Gibraltar</option>' +
                        '<option label="Greece" value="GR">Greece</option>' +
                        '<option label="Greenland" value="GL">Greenland</option>' +
                        '<option label="Grenada" value="GD">Grenada</option>' +
                        '<option label="Guadeloupe" value="GP">Guadeloupe</option>' +
                        '<option label="Guam" value="GU">Guam</option>' +
                        '<option label="Guatemala" value="GT">Guatemala</option>' +
                        '<option label="Guernsey" value="GG">Guernsey</option>' +
                        '<option label="Guinea" value="GN">Guinea</option>' +
                        '<option label="Guinea-Bissau" value="GW">Guinea-Bissau</option>' +
                        '<option label="Guyana" value="GY">Guyana</option>' +
                        '<option label="Haiti" value="HT">Haiti</option>' +
                        '<option label="Heard Island and McDonald Islands" value="HM">Heard Island and McDonald Islands</option>' +
                        '<option label="Holy See (Vatican City State)" value="VA">Holy See (Vatican City State)</option>' +
                        '<option label="Honduras" value="HN">Honduras</option>' +
                        '<option label="Hong Kong" value="HK">Hong Kong</option>' +
                        '<option label="Hungary" value="HU">Hungary</option>' +
                        '<option label="Iceland" value="IS">Iceland</option>' +
                        '<option label="India" value="IN">India</option>' +
                        '<option label="Indonesia" value="ID">Indonesia</option>' +
                        '<option label="Iran, Islamic Republic of" value="IR">Iran, Islamic Republic of</option>' +
                        '<option label="Iraq" value="IQ">Iraq</option>' +
                        '<option label="Ireland" value="IE">Ireland</option>' +
                        '<option label="Isle of Man" value="IM">Isle of Man</option>' +
                        '<option label="Israel" value="IL">Israel</option>' +
                        '<option label="Italy" value="IT">Italy</option>' +
                        '<option label="Jamaica" value="JM">Jamaica</option>' +
                        '<option label="Japan" value="JP">Japan</option>' +
                        '<option label="Jersey" value="JE">Jersey</option>' +
                        '<option label="Jordan" value="JO">Jordan</option>' +
                        '<option label="Kazakhstan" value="KZ">Kazakhstan</option>' +
                        '<option label="Kenya" value="KE">Kenya</option>' +
                        '<option label="Kiribati" value="KI">Kiribati</option>' +
                        '<option label="Korea, Democratic Peoples Republic of" value="KP">Korea, Democratic Peoples Republic of</option>' +
                        '<option label="Korea, Republic of" value="KR">Korea, Republic of</option>' +
                        '<option label="Kuwait" value="KW">Kuwait</option>' +
                        '<option label="Kyrgyzstan" value="KG">Kyrgyzstan</option>' +
                        '<option label="Lao Peoples Democratic Republic" value="LA">Lao Peoples Democratic Republic</option>' +
                        '<option label="Latvia" value="LV">Latvia</option>' +
                        '<option label="Lebanon" value="LB">Lebanon</option>' +
                        '<option label="Lesotho" value="LS">Lesotho</option>' +
                        '<option label="Liberia" value="LR">Liberia</option>' +
                        '<option label="Libyan Arab Jamahiriya" value="LY">Libyan Arab Jamahiriya</option>' +
                        '<option label="Liechtenstein" value="LI">Liechtenstein</option>' +
                        '<option label="Lithuania" value="LT">Lithuania</option>' +
                        '<option label="Luxembourg" value="LU">Luxembourg</option>' +
                        '<option label="Macao" value="MO">Macao</option>' +
                        '<option label="Macedonia, the Former Yugoslav Republic of" value="MK">Macedonia, the Former Yugoslav Republic of</option>' +
                        '<option label="Madagascar" value="MG">Madagascar</option>' +
                        '<option label="Malawi" value="MW">Malawi</option>' +
                        '<option label="Malaysia" value="MY">Malaysia</option>' +
                        '<option label="Maldives" value="MV">Maldives</option>' +
                        '<option label="Mali" value="ML">Mali</option>' +
                        '<option label="Malta" value="MT">Malta</option>' +
                        '<option label="Marshall Islands" value="MH">Marshall Islands</option>' +
                        '<option label="Martinique" value="MQ">Martinique</option>' +
                        '<option label="Mauritania" value="MR">Mauritania</option>' +
                        '<option label="Mauritius" value="MU">Mauritius</option>' +
                        '<option label="Mayotte" value="YT">Mayotte</option>' +
                        '<option label="Mexico" value="MX">Mexico</option>' +
                        '<option label="Micronesia, Federated States of" value="FM">Micronesia, Federated States of</option>' +
                        '<option label="Moldova, Republic of" value="MD">Moldova, Republic of</option>' +
                        '<option label="Monaco" value="MC">Monaco</option>' +
                        '<option label="Mongolia" value="MN">Mongolia</option>' +
                        '<option label="Montenegro" value="ME">Montenegro</option>' +
                        '<option label="Montserrat" value="MS">Montserrat</option>' +
                        '<option label="Morocco" value="MA">Morocco</option>' +
                        '<option label="Mozambique" value="MZ">Mozambique</option>' +
                        '<option label="Myanmar" value="MM">Myanmar</option>' +
                        '<option label="Namibia" value="NA">Namibia</option>' +
                        '<option label="Nauru" value="NR">Nauru</option>' +
                        '<option label="Nepal" value="NP">Nepal</option>' +
                        '<option label="Netherlands" value="NL">Netherlands</option>' +
                        '<option label="Netherlands Antilles" value="AN">Netherlands Antilles</option>' +
                        '<option label="New Caledonia" value="NC">New Caledonia</option>' +
                        '<option label="New Zealand" value="NZ">New Zealand</option>' +
                        '<option label="Nicaragua" value="NI">Nicaragua</option>' +
                        '<option label="Niger" value="NE">Niger</option>' +
                        '<option label="Nigeria" value="NG">Nigeria</option>' +
                        '<option label="Niue" value="NU">Niue</option>' +
                        '<option label="Norfolk Island" value="NF">Norfolk Island</option>' +
                        '<option label="Northern Mariana Islands" value="MP">Northern Mariana Islands</option>' +
                        '<option label="Norway" value="NO">Norway</option>' +
                        '<option label="Oman" value="OM">Oman</option>' +
                        '<option label="Pakistan" value="PK">Pakistan</option>' +
                        '<option label="Palau" value="PW">Palau</option>' +
                        '<option label="Palestinian Territory, Occupied" value="PS">Palestinian Territory, Occupied</option>' +
                        '<option label="Panama" value="PA">Panama</option>' +
                        '<option label="Papua New Guinea" value="PG">Papua New Guinea</option>' +
                        '<option label="Paraguay" value="PY">Paraguay</option>' +
                        '<option label="Peru" value="PE">Peru</option>' +
                        '<option label="Philippines" value="PH">Philippines</option>' +
                        '<option label="Pitcairn" value="PN">Pitcairn</option>' +
                        '<option label="Poland" value="PL">Poland</option>' +
                        '<option label="Portugal" value="PT">Portugal</option>' +
                        '<option label="Puerto Rico" value="PR">Puerto Rico</option>' +
                        '<option label="Qatar" value="QA">Qatar</option>' +
                        '<option label="Reunion" value="RE">Reunion</option>' +
                        '<option label="Romania" value="RO">Romania</option>' +
                        '<option label="Russian Federation" value="RU">Russian Federation</option>' +
                        '<option label="Rwanda" value="RW">Rwanda</option>' +
                        '<option label="Saint Barthelemy" value="BL">Saint Barthelemy</option>' +
                        '<option label="Saint Helena, Ascension and Tristan da Cunha" value="SH">Saint Helena, Ascension and Tristan da Cunha</option>' +
                        '<option label="Saint Kitts and Nevis" value="KN">Saint Kitts and Nevis</option>' +
                        '<option label="Saint Lucia" value="LC">Saint Lucia</option>' +
                        '<option label="Saint Martin" value="MF">Saint Martin</option>' +
                        '<option label="Saint Pierre and Miquelon" value="PM">Saint Pierre and Miquelon</option>' +
                        '<option label="Saint Vincent and the Grenadines" value="VC">Saint Vincent and the Grenadines</option>' +
                        '<option label="Samoa" value="WS">Samoa</option>' +
                        '<option label="San Marino" value="SM">San Marino</option>' +
                        '<option label="Sao Tome and Principe" value="ST">Sao Tome and Principe</option>' +
                        '<option label="Saudi Arabia" value="SA">Saudi Arabia</option>' +
                        '<option label="Senegal" value="SN">Senegal</option>' +
                        '<option label="Serbia" value="RS">Serbia</option>' +
                        '<option label="Seychelles" value="SC">Seychelles</option>' +
                        '<option label="Sierra Leone" value="SL">Sierra Leone</option>' +
                        '<option label="Singapore" value="SG">Singapore</option>' +
                        '<option label="Sint Maarten" value="SX">Sint Maarten</option>' +
                        '<option label="Slovakia" value="SK">Slovakia</option>' +
                        '<option label="Slovenia" value="SI">Slovenia</option>' +
                        '<option label="Solomon Islands" value="SB">Solomon Islands</option>' +
                        '<option label="Somalia" value="SO">Somalia</option>' +
                        '<option label="South Africa" value="ZA">South Africa</option>' +
                        '<option label="South Georgia and the South Sandwich Islands" value="GS">South Georgia and the South Sandwich Islands</option>' +
                        '<option label="South Sudan" value="SS">South Sudan</option>' +
                        '<option label="Spain" value="ES">Spain</option>' +
                        '<option label="Sri Lanka" value="LK">Sri Lanka</option>' +
                        '<option label="Sudan" value="SD">Sudan</option>' +
                        '<option label="Suriname" value="SR">Suriname</option>' +
                        '<option label="Svalbard and Jan Mayen" value="SJ">Svalbard and Jan Mayen</option>' +
                        '<option label="Swaziland" value="SZ">Swaziland</option>' +
                        '<option label="Sweden" value="SE">Sweden</option>' +
                        '<option label="Switzerland" value="CH">Switzerland</option>' +
                        '<option label="Syrian Arab Republic" value="SY">Syrian Arab Republic</option>' +
                        '<option label="Taiwan" value="TW">Taiwan</option>' +
                        '<option label="Tajikistan" value="TJ">Tajikistan</option>' +
                        '<option label="Tanzania, United Republic of" value="TZ">Tanzania, United Republic of</option>' +
                        '<option label="Thailand" value="TH">Thailand</option>' +
                        '<option label="Timor-Leste" value="TL">Timor-Leste</option>' +
                        '<option label="Togo" value="TG">Togo</option>' +
                        '<option label="Tokelau" value="TK">Tokelau</option>' +
                        '<option label="Tonga" value="TO">Tonga</option>' +
                        '<option label="Trinidad and Tobago" value="TT">Trinidad and Tobago</option>' +
                        '<option label="Tunisia" value="TN">Tunisia</option>' +
                        '<option label="Turkey" value="TR">Turkey</option>' +
                        '<option label="Turkmenistan" value="TM">Turkmenistan</option>' +
                        '<option label="Turks and Caicos Islands" value="TC">Turks and Caicos Islands</option>' +
                        '<option label="Tuvalu" value="TV">Tuvalu</option>' +
                        '<option label="Uganda" value="UG">Uganda</option>' +
                        '<option label="Ukraine" value="UA">Ukraine</option>' +
                        '<option label="United Arab Emirates" value="AE">United Arab Emirates</option>' +
                        '<option label="United Kingdom" value="GB">United Kingdom</option>' +
                        '<option label="United States" value="US">United States</option>' +
                        '<option label="United States Minor Outlying Islands" value="UM">United States Minor Outlying Islands</option>' +
                        '<option label="Uruguay" value="UY">Uruguay</option>' +
                        '<option label="Uzbekistan" value="UZ">Uzbekistan</option>' +
                        '<option label="Vanuatu" value="VU">Vanuatu</option>' +
                        '<option label="Venezuela, Bolivarian Republic of" value="VE">Venezuela, Bolivarian Republic of</option>' +
                        '<option label="Viet Nam" value="VN">Viet Nam</option>' +
                        '<option label="Virgin Islands, British" value="VG">Virgin Islands, British</option>' +
                        '<option label="Virgin Islands, U.S." value="VI">Virgin Islands, U.S.</option>' +
                        '<option label="Wallis and Futuna" value="WF">Wallis and Futuna</option>' +
                        '<option label="Western Sahara" value="EH">Western Sahara</option>' +
                        '<option label="Yemen" value="YE">Yemen</option>' +
                        '<option label="Zambia" value="ZM">Zambia</option>' +
                        '<option label="Zimbabwe" value="ZW">Zimbabwe</option>' +
                        '</select>' +
                        '<input id="input_ssl_stateOrProvince" class="form-control generateSSL" placeholder="State or Province">' +
                        '<input id="input_ssl_locality" class="form-control generateSSL" placeholder="Locality Name">' +
                        '<input id="input_ssl_org" class="form-control generateSSL" placeholder="Organization Name">' +
                        '<input id="input_ssl_orgUnit" class="form-control generateSSL" placeholder="Orgazniational Unit Name">' +
                        '<input id="input_ssl_email" class="form-control generateSSL" placeholder="Email Address">' +
                        '<a id="ssl_cert_prev_btn" href="#" ui-sref="">Previous</a>' +
                        '<a href="#" ui-sref="state4" onclick = "generateSSLCertificate()">Generate</a>' +
                        '</div>' +
                        '</div>',
                        controller: function($rootScope) {
                            $rootScope.homePageIsShown = false;
                            checkAccess('state4');
                        }
                    }
                }
            })


            .state('download', {
                url: "/download",
                data: {
                    prev: 'home'
                },
                views: {
                    'input-views': {
                        template: '<div class="inner-wrapper">' +
                        '<div class="slide-header">Download</div>' +
                        '<div class="slide-desc">Here is the SSL Certificate you currently have</div>' +
                        '<div class="slide-ssl-info">' +
                        'Domain :'  + '<div id = "download_ssl_domain" class=""> </div>' +
                        '<br>' +
                        'Issued Date : '+ '<div id = "download_issueddate" class=""> </div>' +
                        '</div>' +
                        '<div class="button-wrapper">' +
                        '<a href="#" ui-sref="home">Previous</a>' +
                        '<a href="#" ui-sref="home" onclick = "downloadCertificate()">Download</a>' +
                        '<a href="#" ui-sref="state1">Create new</a>' +
                        '</div>' +
                        '</div>',
                        controller: function($rootScope) {
                            getSSLCertainfo();
                            $rootScope.homePageIsShown = false;
                            checkAccess('download');
                        }
                    }
                }
            })
    })
    .controller('MainController', function($rootScope, $scope) {
        $rootScope.homePageIsShown = true;
        $scope.state = {};
        $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState) {
            var prev = fromState.data ? fromState.data.prev : '';
            $scope.state.back = (toState.name === prev);
            $scope.state.toHome = (toState.name === 'home');
            $scope.$apply();
        });
    });


function toggleSslCertTable()
{
    alert('topggle class ');
    jQuery(document).ready(function ($) {
        $("sslCertTable").toggle();
    });

}

//STEP 1: CHECK IF THE DOMAIN IS ALREADY VALIDATED
//IF VALIDATED : SHOW THE GENERATE SSL CERTIFICATE SLIDE
//ELSE : SHOW THE DOMAIN VALIDATION SLIDE
function checkDomainStatus()
{
    showLoading('Please Wait .......');
    jQuery(document).ready(function ($) {

        $.ajax({
            type: "GET",
            url: url,
            dataType: 'json',
            data: {
                option: option,
                controller: controller,
                action: 'checkDomainValidationStatus',
                task: 'checkDomainValidationStatus',
                centnounce: $('#centnounce').val()
            },
            success: function (data) {
                if(data.status == 1)
                {
                    //domain has been validated
                    //go to ssl cert slide
                    //CHECK IF THE SSL CERTIFICATE HAS BEEN GENERATED
                    //IF GENERATED ASK USER TO DOWNLOAD THE CERTIFICATE
                    checkSSLCert();

                }else if(data.status == 2){
                    hideLoading();
                    $("#ssl_download_btn").css('display','none');
                    $("#ssl_start_btn").attr('href','#/state1');
                    $("#ssl_cert_prev_btn").attr('href','#/state3');
                    //$("#ssl_cert_prev_btn").attr('ui-sref','state3');
                    //$("#ssl_cert_prev_btn").css('display','none');
                    //domain is not validates
                    //show validate domain view
                    //HIDE THE DOWNLOAD BUTTON AND SHOW THE CREATE NEW

                }else if(data.status == 3)
                {
                    // re validate the domain
                    hideLoading();
                    $("#ssl_download_btn").css('display','none');
                    $("#ssl_start_btn").attr('href','#/state1a');
                    $("#ssl_cert_prev_btn").attr('href','#/state3a');
                }
                else {
                    hideLoading();
                    //error in cheking domain status
                    showDialogue(data.info,'ERROR','CLOSE');
                }
            }
        })
    });
}

function  getDomain()
{
    jQuery(document).ready(function ($) {
        $.ajax({
            type: "GET",
            url: url,
            dataType: 'json',
            data: {
                option: option,
                controller: controller,
                action: 'getDomain',
                task: 'getDomain',
                centnounce: $('#centnounce').val()
            },
            success: function (data) {
                domain = data.root;
                subdomain = data.subdomain;
                $("#select_ssl_domain").html(domain);
            }
        })
    });
}

//STEP 2: IF THE DOMAIN HAS NOT BEEN VALIDATED
//SHOW THE LIST OF EMAIL IDS AND AKS USER TO SELECT ONE
function queryEmailForValidation(revalidate)
{
    showLoading('Retrieving the list of email ids, Please Wait ...');
    jQuery(document).ready(function ($) {
        $.ajax({
            type: "GET",
            url: url,
            dataType: 'json',
            data: {
                option: option,
                controller: controller,
                action: 'queryEmailForValidation',
                task: 'queryEmailForValidation',
                centnounce: $('#centnounce').val()
            },
            success: function (data) {
                hideLoading();
                if(data.status == 1)
                {
                //data.info.length;
                //use data.info array to show the list of email ids
                //length = data.info.len()
                //elements can be accessed by

                  if(revalidate == 1)
                  {
                      //go to the revalidate send code slide
                      window.location = base_url+'state2a';
                      //var emailArray = data.info;
                      //var option = '';
                      //for (var i=0;i<emailArray.length;i++){
                      //    option += '<option value="'+ emailArray[i] + '">' + emailArray[i] + '</option>';
                      //}
                      //$('#select_ssl_email').append(option);

                  }else {
                      //send validatoion slide
                      window.location = base_url+'state2';
                      //var emailArray = data.info;
                      //var option = '';
                      //for (var i=0;i<emailArray.length;i++){
                      //    option += '<option value="'+ emailArray[i] + '">' + emailArray[i] + '</option>';
                      //}
                      //$('#select_ssl_email').append(option);
                  }
                    var emailArray = data.info;
                    var option = '';
                    for (var i=0;i<emailArray.length;i++){
                        option += '<option value="'+ emailArray[i] + '">' + emailArray[i] + '</option>';
                    }
                    $('#select_ssl_email').append(option);
                }else {
                    showDialogue(data.info,'ERROR','CLOSE');
                    //go back to the first slide where we ask user to validate domain
                }
            }
        })
    });
}
//STEP 3 : AFTER SELECTING THE EMAIL ADDRESS
//CODE WILL BE SENT TO THE SELECTED EMAIL ADDRESS
function sendVerificationCode()
{
    showLoading('Sending verification code to the email. Please Wait ... ');
    jQuery(document).ready(function ($) {
        $.ajax({
            type: "POST",
            url: url,
            dataType: 'json',
            data: {
                option: option,
                controller: controller,
                action: 'sendVerificationCode',
                task: 'sendVerificationCode',
                email : $("#select_ssl_email").val(),
                centnounce: $('#centnounce').val()
            },
            success: function (data) {
                hideLoading();
                if(data.status == 1)
                {
                    //ASK USER TO CHECK EMAIL ADDRESS AND INSERT THE CODE
                    showDialogue(data.info,'UPDATE','CLOSE');
                    //$("#send_verification_button").attr('href','#/state3');
                    //send_verification_button
                }else {
                    //ERROR GO BACK TO START OF THE VALIDATION SLIDE
                    //verification_code_button
                    $("#verification_code_bbutton").click();
                    showDialogue(data.info,'ERROR','CLOSE');

                }
            }
        })
    });
}

//STEP 3A: SEND RE VALIDATION CODE
//USED TO REVALIDATE A DOMAIN
function sendReVerificationCode()
{
    showLoading('Sending Re validation code to the email. Please Wait ... ');
    jQuery(document).ready(function ($) {
        $.ajax({
            type: "POST",
            url: url,
            dataType: 'json',
            data: {
                option: option,
                controller: controller,
                action: 'sendDomainValidationAuthCode',
                task: 'sendDomainValidationAuthCode',
                email : $("#select_ssl_email").val(),
                centnounce: $('#centnounce').val()
            },
            success: function (data) {
                hideLoading();
                if(data.status == 1)
                {
                    //ASK USER TO CHECK EMAIL ADDRESS AND INSERT THE CODE
                    showDialogue(data.info,'UPDATE','CLOSE');
                    //$("#send_verification_button").attr('href','#/state3');
                    //send_verification_button
                }else {
                    //ERROR GO BACK TO START OF THE VALIDATION SLIDE
                    //verification_code_button
                    $("#verification_code_bbutton").click();
                    showDialogue(data.info,'ERROR','CLOSE');

                }
            }
        })
    });
}



//STEP 4: AFTER INSERTING THE CODE
//VALIDATE THE DOMAIN
function validateDomain()
{
    showLoading('Please wait.......validation the domain now');
    jQuery(document).ready(function ($) {
        authcode = $("#input_ssl_validation").val();
        $.ajax({
            type: "GET",
            url: url,
            dataType: 'json',
            data: {
                option: option,
                controller: controller,
                action: 'validateDomain',
                task: 'validateDomain',
                authcode : authcode,
                centnounce: $('#centnounce').val()
            },
            success: function (data) {
                hideLoading();
                if(data.status == 1)
                {
                    //THE DOMAIN WAS SUCCESSFULLY VALIDATED
                    //SHOW THE UPDATE TO THE USER AND SHOW THE GENERATE SSL CERT PAGE
                     window.location = base_url+'state4';
                    showDialogue(data.info,'UPDATE','CLOSE');


                }else {
                    //ERROR IN VALIDATING THE DOMAIN
                    //$("#verification_code_nbutton").click();
                    showDialogue(data.info,'ERROR','CLOSE');
                }
            }
        })
    });
}

//STEP 4A: RE VALIDATE THE DOMIN
//CHECK IF THE AUTH CODE MATCHES
function reValidateDomain()
{
    showLoading('Please wait.......Re validation the domain now');
    jQuery(document).ready(function ($) {
        authcode = $("#input_ssl_validation").val();
        $.ajax({
            type: "GET",
            url: url,
            dataType: 'json',
            data: {
                option: option,
                controller: controller,
                action: 'revalidateDomain',
                task: 'revalidateDomain',
                authcode : authcode,
                centnounce: $('#centnounce').val()
            },
            success: function (data) {
                hideLoading();
                if(data.status == 1)
                {
                    //THE DOMAIN WAS SUCCESSFULLY VALIDATED
                    //SHOW THE UPDATE TO THE USER AND SHOW THE GENERATE SSL CERT PAGE
                    window.location = base_url+'state4';
                    showDialogue(data.info,'UPDATE','CLOSE');


                }else {
                    //ERROR IN VALIDATING THE DOMAIN
                    //$("#verification_code_nbutton").click();
                    showDialogue(data.info,'ERROR','CLOSE');
                }
            }
        })
    });
}




//FUNCTION TO CHECK IF THE SSL CERTIFICATE EXISTS IN THE DB AND THE LOCAL FILS
//STEP 5: GENERATE SSL CERTIFICATE
//DOMAIN NEEDS TO BE VALIDATED TO GENERATE A CERTIFICATE
function generateSSLCertificate()
{
    showLoading('Generating ssl certificate \n Please Wait.........');
    jQuery(document).ready(function ($) {
        countryName = $("#input_ssl_country").val();
        stateOrProvinceName = $("#input_ssl_stateOrProvince").val();
        localityName = $("#input_ssl_locality").val();
        organizationName = $("#input_ssl_org").val();
        organizationalUnitName = $("#input_ssl_orgUnit").val();
        emailAddress = $("#input_ssl_email").val();
        $.ajax({
            type: "GET",
            url: url,
            dataType: 'json',
            data: {
                option: option,
                controller: controller,
                action: 'genSSLCertificate',
                task: 'genSSLCertificate',
                countryName : countryName,
                stateOrProvinceName: stateOrProvinceName,
                localityName :localityName,
                organizationName :organizationName,
                organizationalUnitName :organizationalUnitName,
                domain :domain,
                emailAddress : emailAddress,
                centnounce: $('#centnounce').val()
            },
            success: function (data) {
                if(data.status == 1)
                {
                   hideLoading();
                   showLoading('Preparing to download the SSL Certificate');
                   window.location = base_url+'download';
                }else
                {
                    hideLoading();
                    showDialogue(data.info, 'ERROR','CLOSE');
                }
            }
        })
    });
}

function downloadCertificate()
{
    showLoading('Preparing to Download, Please wait....');
    jQuery(document).ready(function ($) {
        $.ajax({
            type: "GET",
            url: url,
            dataType: 'json',
            data: {
                option: option,
                controller: controller,
                action: 'retrieveSSLCertificate',
                task: 'retrieveSSLCertificate',
                centnounce: $('#centnounce').val()
            },
            success: function (data) {
                hideLoading();
                if(data.status == 1)
                {
                    var win = window.open(data.info, '_blank');
                    win.focus();
                    window.location.reload();
                    window.location = base_url+'home';
                }
                else if(data.status == 0)
                {
                    showDialogue(data.info, 'ERROR','CLOSE');
                }

            }
        })
    });
}

function checkSSLCert()
{
    jQuery(document).ready(function ($) {
        $.ajax({
            type: "GET",
            url: url,
            dataType: 'json',
            data: {
                option: option,
                controller: controller,
                action: 'checkSSLCert',
                task: 'checkSSLCert',
                centnounce: $('#centnounce').val()
            },
            success: function (data) {
                //0 => generate new certificate
                //1 => no need to generate new certificates
               if(data.status == 0)
               {
                   hideLoading();
                   //$("#ssl_start_btn").attr('ui-sref','state4');
                   $("#ssl_download_btn").css('display','none');
                   $("#ssl_start_btn").attr('href','#/state4');
                   $("#ssl_cert_prev_btn").attr('href','#/home');
                   //window.location = base_url+'state4';
                   //showDialogue(data.info,'UPDATE','CLOSE');
                   //allow user to generate a new certificate
                   //show slides to generate the ssl certificate
                    //DISABLE THE DOWNLOAD BUTTON AND SHOW CREATE NEW BUTTON TO SHOW THHE FORM
               }
                if(data.status == 1)
                {
                    hideLoading();
                    $("#ssl_start_btn").css('display','none');
                    //showDialogue(data.info,'UPDATE','CLOSE');
                    //certificates are upto date
                    //don not allow user to generate a certificate
                    //just allow them to download the certificate
                    //DISABLE THE CREATE NEW BUTTON
                }
            }
        })
    });
}

function getSSLCertainfo()
{
    jQuery(document).ready(function ($) {
        $.ajax({
            type: "GET",
            url: url,
            dataType: 'json',
            data: {
                option: option,
                controller: controller,
                action: 'getSSLCertainfo',
                task: 'getSSLCertainfo',
                centnounce: $('#centnounce').val()
            },
            success: function (data) {
                if(data.status == 1)
                {
                    $("#download_ssl_domain").html(data.info.ssl_domain);
                    $("#download_issueddate").html(data.info.issued_date);
                }else {
                    //showDialogue('There was some problem in accessing the SSL Certificate information' , 'ERROR','CLOSE');
                }
            }
        })
    });
}

jQuery(document).ready(function ($) {
var choices = $('#switch-ssl-domain .choice')
var choice_select = $('.select_ssl_domain.choice')
var choices_new = $('.new_ssl_domain.choice')
    , text = $('#switch-ssl-domain .or')

    choices
        .on('click', function(){
            choices.toggleClass('on')
            text.addClass('flip')
            setTimeout(function(){
                text.removeClass('flip')
            }, 1000)
        });

    choice_select
        .on('click', function(){
            $("#select_ssl_domain").css('display','block');
            $("#select_ssl_path").css('display','block');
            $("#new_ssl_domain").css('display','none');
            $("#new_ssl_path").css('display','none');
        });

    choices_new
        .on('click', function(){
            $("#select_ssl_domain").css('display','none');
            $("#select_ssl_path").css('display','none');
            $("#new_ssl_domain").css('display','block');
            $("#new_ssl_path").css('display','block');
        })
});
function checkAccess(state)
{
    jQuery(document).ready(function ($) {
        $.ajax({
            type: "GET",
            url: url,
            dataType: 'json',
            data: {
                option: option,
                controller: controller,
                action: 'checkAccess',
                task: 'checkAccess',
                state : state,
                centnounce: $('#centnounce').val()
            },
            success: function (data) {
                if(data.status == 0)
                {
                    window.location = base_url+'home';
                }
            }
        })
    });
}


function getBaseUrl()
{
    jQuery(document).ready(function ($) {
        $.ajax({
            type: "GET",
            url: url,
            dataType: 'json',
            data: {
                option: option,
                controller: controller,
                action: 'getBaseUrl',
                task: 'getBaseUrl',
                centnounce: $('#centnounce').val()
            },
            success: function (data) {
                if(data.data == 'wordpress')
                {
                    base_url = "admin.php?page=ose_fw_ssl#/";
                }else {
                    base_url = "index.php?option=com_ose_firewall&view=ssl#/";
                }
            }
        })
    });
}


