<?php
require_once(dirname(__FILE__).'/BaseTest.php');
class GitbackupTest extends BaseTest
{
    protected function setPosttest ($action) {
        $_REQUEST['action'] = $action;
        $_REQUEST['qatest'] = true;
    }
    protected function setPostVariablesforTets ($key, $value) {
        $_REQUEST[$key] = $value;
    }
    protected function systemTest () {
        ini_set("display_errors", "on");
        $ready = oseFirewall::preRequisitiesCheck();
        $this->showHeader("Checking System is Ready");
        $this->assertEquals('1', $ready);
        $this->showResults(true, 'Checking System is Ready: expecting 1, actual: '. $ready);

    }
    public function testSystemReady()
    {
        $this->systemTest();
//         $this->Enablegitbackup();
        // $this->GetGitLog();
        // $this->GetLastBackupTime();
        // $this->GitCloudCheck();
        // $this->CreateBackupAllFiles();
        // $this->FindChanges();

//        $this->BackupDbTest();
//        $this->ContBackupDb();
//        $this->LocalBackup();
//        $this->GitRollbackWithDb();
        $this->GetZipUrl();
    }

    //test case to check if the git inttialization works or not
    public function Enablegitbackup()
    {
        $action  = 'enableGitBackup';
        $this->setPosttest($action);
        $result = $this->oseFirewall->testController('gitbackupController', $action);
        $this->assertEquals('1', $result['status']);
        $this->showResults(true, 'Checking the Enablegitbackup function , expected : 1 ; actual: '.$result['status']);
    }

    //test case for checking the git log from db
    //check if the git log and the log stored in the db have the same count for the commit ids
    public function GetGitLog()
    {
        $action = 'getGitLog';
        $this->setPosttest($action);
        $gitsetup = $this->loadGitsetupLibrary();
        $commitcountfromlog = count($gitsetup->getCommitIDFromGitLog());
        $commitcountfromdb = $gitsetup->getCommitCountFromDb();
        if($commitcountfromdb == $commitcountfromlog)
        {
            $flag = true;
            $this->assertTrue($flag);
            $this->showResults(true, 'Checking the GetGitLog function, record count from db : '.$commitcountfromdb.' count from log '.$commitcountfromlog);
        }
        else
        {
           $flag = false;
           echo "Count from the git log is ".$commitcountfromlog ;
           echo"<br>";
           echo "Count from the Db is ".$commitcountfromdb;
            $this->assertTrue($flag);

        }
    }

    // test case to cehck if the system can get the last backup time
    public function GetLastBackupTime()
    {
        $action = 'getLastBackupTime';
        $this->setPosttest($action);
        $result = $this->oseFirewall->testController('gitbackupController', $action);
        $this->assertNotNull($result['commit_time']);
        $this->showResults(true, 'Checking the GetLastBackupTime function, expected : Should not be Null ; actual: '.$result['commit_time']);
    }

//    public function setCommitMessage()
//    {
//        $action = 'setCommitMessage';
//        $this->setPosttest($action);
//        $result = $this->oseFirewall->testController('gitbackupController', $action);
//        $this->assertEquals('1',$result['commit_time']);
//        $this->showResults(true, 'Checking the Set commit message, expected : Should not be Null ; actual: '.$result['commit_time']);
//
//    }

    //test case to check if remote repository is set or not
    public function GitCloudCheck()
    {
        $action = "gitCloudCheck";
        $this->setPosttest($action);
        $result = $this->oseFirewall->testController('gitbackupController', $action);
        $this->assertEquals(true, $result);
        $this->showResults(true, 'Checking the gitCloudCheck function , expected : true ; actual: '.$result);
    }

    //test case to check discard changes
    public function DiscardChanges()
    {
        $action = 'discardChanges';
        $this->setPosttest($action);
        $result = $this->oseFirewall->testController('gitbackupController', $action);
        $this->assertEquals('1', $result['status']);
        $this->showResults(true, 'Checking the DiscardChanges function , expected : true ; actual: '.$result['status']. "Error : ".$result['info']);
    }

    //test case to get the zip url
    public function GetZipUrl()
    {
        $action = 'getZipUrl';
        $this->setPosttest($action);
        $result = $this->oseFirewall->testController('gitbackupController', $action);
        $this->assertNotNull($result['url']);
        $this->showResults(true, 'Checking the GetZipUrl function, expected : Not Null ; actual: '.$result['url']);
    }

    //works only if you have any chnages, if there are no chnages it will fgail and have result['status']=> 0
    public function FindChanges()
    {
        $action = 'findChanges';
        $this->setPosttest($action);
        $result = $this->oseFirewall->testController('gitbackupController', $action);
        $this->assertNotNull($result['status']);
        $this->showResults(true, 'Checking the FindChanges function , expected : 1 ; actual: '.$result['status']);
    }

    //Pass only if the zip file exists
    public function DeleteZipBakcupFile()
    {
        $action = 'deleteZipBakcupFile';
        $this->setPosttest($action);
        $result = $this->oseFirewall->testController('gitbackupController', $action);
        $this->assertEquals('1', $result['status']);
        $this->showResults(true, 'Checking the DeleteZipBakcupFile function , expected : 1 ; actual: '.$result['status']);

    }

    //commit message can be null or can contain some text
    public function SetCommitMessage()
    {
        $action = 'setCommitMessage';
        $this->setPosttest($action);
        $commit_message = "QUALITY ASSURANCE TESTING : DO NOIT USE THIOS COMMIT TO ROLLBACK ";
        $this->setPostVariablesforTets('commitmessage', $commit_message);
        $result = $this->oseFirewall->testController('gitbackupController', $action);
        $this->assertEquals('1', $result['status']);
        $this->showResults(true, 'Checking the SetCommitMessage function , expected : 1 ; actual: '.$result['status']);
    }

    public function chooseRandomCommitId()
    {
        $action = 'chooseRandomCommitId';
        $this->setPosttest($action);
        $result = $this->oseFirewall->testController('gitbackupController', $action);
        return $result;
//        $this->assertNotNull($result['value']);
//        $this->showResults(true, 'Checking the chooseRandomCommitId function , expected : Not Null ; actual: '.$result);
    }
    //return random commit id from  the git log
    public function chooseRandomCommitIdTest()
    {
        $action = 'chooseRandomCommitId';
        $this->setPosttest($action);
        $result = $this->oseFirewall->testController('gitbackupController', $action);
        $this->assertNotNull($result['value']);
        $this->showResults(true, 'Checking the chooseRandomCommitId function , expected : Not Null ; actual: '.$result);
    }
    //only passes for premium users
    public function ViewChangeHistory($commitid)
    {
        $action = 'viewChangeHistory';
        $this->setPosttest($action);
        $this->setPostVariablesforTets('commitid',$commitid);
        $temp = $this->oseFirewall->testController('gitbackupController', $action);
        $this->assertEquals('1', $temp['status']);
        $this->showResults(true, 'Checking the ViewChangeHistory function , expected : 1 ; actual: '.$temp['status']);
    }

    //to check if the zip file of the website was generated successfully
    public function WebsiteZipBackup()
    {
        $action = 'websiteZipBackup';
        $this->setPosttest($action);
        $temp = $this->oseFirewall->testController('gitbackupController', $action);
        $this->assertEquals('1', $temp['status']);
        $this->showResults(true, 'Checking the WebsiteZipBackup function , expected : 1 ; actual: '.$temp['status']);
    }

    //Do not use
//    public function Downloadzip()
//    {
//        $action = 'downloadzip';
//        $this->setPosttest($action);
//        $result = $this->oseFirewall->testController('gitbackupController', $action);
//        $nofile = "no backup file" ;
//        $this->assertNotEquals($nofile, $result);
//        $this->showResults(true, 'Checking the Downloadzip function , expected : 1 ; actual: '.$result);
//    }

    //functionality to check backupdb function
    //shpuld generate dbtable file and .sql files and should also return the name of the backed up table
    public function BackupDbTest()
    {
        $action = 'backupDB';
        $this->setPosttest($action);
        $result = $this->oseFirewall->testController('gitbackupController', $action);
        $returned_table= false;

        //MAKE SURE YOU HAVE RECEIVED THE NAME OF THE BACKED UP TABLE
        if(!empty($result['table']))
        {
            $returned_table = true;
        }

        oseFirewallBase::callLibClass('gitBackup','GitSetup');
        $gitsetup = new GitSetup();
        //CHECK IF THE DBTABLES.PHP EXISTS
        $tablelist =OSE_FWDATA . ODS . "backup" . ODS . "dbtables.php";
        $dbtableliestexists = $gitsetup->fileExists($tablelist);

        //CHECK IF CORRESPONDING .SQL FILES EXISTS FOR THE ENTRIES IN THE DBTABLES.PHP FILE
        $sqlfilesexists = $this->CheckSqlFileExistsForEachBackedUpTables();

//        echo "the backed up table list is ";
//        print_r($backeduptableslist); exit;
//        echo "the sql file flag is ";
//        print_r($sqlfilesexists);

        $flag = $dbtableliestexists && $sqlfilesexists && $returned_table;
        $this->assertTrue($flag);
        $this->showResults(true, 'Checking the BackupDbTest function , expected : True ; actual: '.$flag);
    }

    //checks if for each entry in the backed up array their sql files exist or not
    public function CheckSqlFileExistsForEachBackedUpTables()
    {
        $gitsetup = $this->loadGitsetupLibrary();
        $backeduptableslist = $gitsetup->getListOfBackedUpTablesFromTheFileList();
        $sqlfilesexists = false;
        if(empty($backeduptableslist))
        {
            $sqlfilesexists = false;
        }
        else {
            foreach($backeduptableslist as $value)
            {
                if($gitsetup->checkSQLFileExists($value))
                {
                    $sqlfilesexists = true;
                }
                else {
                    $sqlfilesexists = false;
                }
            }
        }
        return $sqlfilesexists;
    }




    //test case to create a remote repo and setting it up
    /*
     * PRE-REQUISITE - need to have a bitbucket account , the test case already has username and password  // NEEDS TPO BE HARDCODED BELOW
     * by default the repo name is qatest  => needs to be changed manually or will cause errors
     * IMPORTAANT : delete all the public and private keys before performing this test and unlink the remote repo
     *  this can be done calling method clearSaveRempteRepoTesting()
     */
    public function SaveRemoteGit()
    {
        oseFirewallBase::callLibClass('gitBackup','GitSetup');
        $gitsetup = new GitSetup();
        $action = 'saveRemoteGit';
        $this->setPosttest($action);
        $username = "centroratest";
        $passwword = "centroratest2016";
        $this->setPostVariablesforTets('username',$username);
        $this->setPostVariablesforTets('password',$passwword);
        $result = $this->oseFirewall->testController('gitbackupController', $action);
//        print_r($result);
        $this->assertContains('SUCCESS',$result['status']);
        $this->showResults(true, 'Checking the SaveRemoteGit function , expected : SUCCESS ; actual: '.$result['status']);
    }
    
    //IMPORTASNT : needs to be executed to remove chnages to the clients repo for testing
    public function clearSaveRempteRepoTesting()
    {
        $username = "centroratest";   //test  username
        $passwword = "centroratest2016"; //test
        oseFirewallBase::callLibClass('gitBackup','GitSetup');
        $gitsetup = new GitSetup();
        $gitsetup->deletePrivateKey();
        $gitsetup->deletePublicKey();
        $gitsetup->moveKeyAfterTest();
        $gitsetup->removeremoterepo("qatestrepo");
        $gitsetup->deleterepoonBitbucket($username, $passwword);
    }
    public function loadGitsetupLibrary()
    {
        oseFirewallBase::callLibClass('gitBackup','GitSetup');
		$gitsetup = new GitSetup(true);
        return $gitsetup;
    }

    /*The test checks if the commit was successfull
    the test case checks for 3 conditions
    1. if the returned status == 1 => the command was successfull
    2. get the head from git log
    3. get the head from the databse tables
    if 2 and 3 are same that means commit was successfull
     *
     */
    public function CreateBackupAllFiles()
    {
        $gitsetup = $this->loadGitsetupLibrary();
        $action = 'createBackupAllFiles';
        $this->setPosttest($action);
        $result  = $this->oseFirewall->testController('gitbackupController', $action);   //result['status']  == 1
        $headfromgitlog = $gitsetup->getHeadFromGitLog();    //$headfromgitlog  ==same as head in db
        $headfromdb = $gitsetup->getHead();
        $headfromdb_commitid = $headfromdb[0]['commit_id'];
        if($result['status'] == 1 &&(strcmp($headfromdb_commitid, (string)$headfromgitlog['commitid']) == 0))
        {
            $flag = true ;
        }
        else
        {
            $flag = false;
        }
        $this->assertTrue($flag);
        $this->showResults(true, 'Checking the CreateBackupAllFiles function , expected : True  ; actual: '.$flag);
    }

//    public function GitRollback()
//    {
//
//        $commandoutput = false;
//        $samecommitid = false;
//        $gitsetup = $this->loadGitsetupLibrary();
//        $recall = "old";
//        $action = 'gitRollback';
//        $this->setPosttest($action);
//        $commitid = "14a909b";
//        $this->setPostVariablesforTets('commitHead',$commitid);
//        $this->setPostVariablesforTets('recall',$recall);
//        $result = $this->oseFirewall->testController('gitbackupController', $action);
//        //head should be the same
//        $headfromgitlog = $gitsetup->getHeadFromGitLog();
//        $headfromgitlog_commitid = (string)$headfromgitlog['commitid'];
//        if($result['status'] == 1 && (strcmp($headfromgitlog_commitid, (string)$commitid)== 0))
//        {
//            $flag = true;
//        }
//        else {
//            $flag = false;
//        }
//        $this->assertTrue($flag);
//        $this->showResults(true, 'Checking the GitRollback function , expected : true ; actual: '.$flag);
//    }

    /*Gitrollback to check if the system can rollback with the chnages in a file
     * 1.get the current head, we will be reverting back to this head to avoid any changes on the cliets website
     * 2.create a blan php file
     * 3.rollback => this will commit the chnages and will try to revert back to the head as in step 1
     */
    public function GitRollbackWithTestFile()
    {
        $gitsetup = $this->loadGitsetupLibrary();
        $action = 'gitRollback';
        $this->setPosttest($action);
        $headfromgitlog = $gitsetup->getHeadFromGitLog(); // geth the current head
        $headfromgitlog_commitid = (string)$headfromgitlog['commitid'];
        $this->setPostVariablesforTets('commitHead',$headfromgitlog_commitid);
        $this->setPostVariablesforTets('recall','old'); // we are choosing old db for testing
        $result = $this->addblankfile();
//        echo "Added blank file is ".$result."<br/>";
        if($result)  //blank file added
        {
            $temp = $this->oseFirewall->testController('gitbackupController', $action);
//            echo "Rollback result is ";
//            print_r($temp);
//            echo "<br/>";
            $fileexist = file_exists(OSE_QATESTFILE); //should be false after the rollback
            if($temp['status'] == 1 && $fileexist == false)
            {
                $flag = true;
                $this->assertTrue($flag);
                $this->showResults(true, 'Checking the GitRollbackWithTestFile function , expected : True ; actual: '.$flag);

            }else
            {
                $this->assertTrue(false);
            }
        }else {
            $this->assertTrue(false);
        }
    }

    /*TO CHECK IF THE ROLLBACK WORKS FINE FOR THE DB AS WELL
    1. TO TEST THIS WE CREATE A ENTRY IN THE secConfig TABLE
    2.ROLL BACK = > WHICH WILL COMMIT THE ENTRY IN DB AND ROLLBACK TO THE INITIAL STAGE WHERE THE DB ENTRY WAS NOT PRESENT
    3. CHECK IF THE STATUS FROM THE CONTROLLER WAS 1 AND IF THE ENTRY IN DB IS NOT PRESENT
     */
    public function GitRollbackWithDb()
    {
        $gitsetup = $this->loadGitsetupLibrary();
        $action = 'gitRollback';
        $this->setPosttest($action);
        $headfromgitlog = $gitsetup->getHeadFromGitLog(); // geth the current head
        $headfromgitlog_commitid = (string)$headfromgitlog['commitid'];
        $this->setPostVariablesforTets('commitHead',$headfromgitlog_commitid);
        $this->setPostVariablesforTets('recall','old'); // we are choosing old db for testing
        $result = $gitsetup->createtestDbEntry();
        echo "the db has been added ".$result."<br/>";
        if($result != 0)  // record added successfully
        {
            $temp = $this->oseFirewall->testController('gitbackupController', $action);
//            echo "the roll;back result is ";
//            print_r($temp);
//            echo "<br/>";
            $dbrecordexist = $gitsetup->testDbEntryExists(); //should be false after the rollback
//            echo "does the db exists after rollback ". $dbrecordexist."<br/>";
            if($temp['status'] == 1 && $dbrecordexist == false)
            {
                $flag = true;
                $this->assertTrue($flag);
                $this->showResults(true, 'Checking the GitRollbackWithDb function , expected : True ; actual: '.$flag);

            }
            else {
                $this->assertTrue(false);
            }
        }else {
            $this->assertTrue(false);

        }
    }


    /*To check the push functionality
     * WARNING : PLEASE MAKE SURE YOU SET THE $branchname properly, it should be master on the client side
     * PREREQUISITE : run the save remote git test first to setup the remote repo and make sure you COMMIT ALL THE CHNAGES BEFORE RUNNING THIS METHOD
     * it will return error = 2 if you have uncommitted changes
     * step 1.check if remote repo is set
     * step 2. run the controller
     * step 3. get the head from git log
     * step 4. get the head of remotge repo
     * step 5. if commit id from 3 and 4 are same the pass else fails
     */
    public function GitCloudPush()
    {
        $action = 'gitCloudPush';
        if(TEST_ENV)
        {
            $branchname = "6.4.0" ;
        }else
        {
            $branchname = "master" ;
        }
        $remotereponame = "qatestrepo";

        $gitsetup = $this->loadGitsetupLibrary();

        $this->setPosttest($action);
        $remoterepo = $gitsetup->getRemoteRepoUrl('qatestrepo');
//        echo "remote repos url is ";
//        print_r($remoterepo);
//        echo "<br/>";
        if($remoterepo['status'] == 1)
        {
            $temp = $this->oseFirewall->testController('gitbackupController', $action);
//            echo "the controller result is ";
//            print_r($temp);
//            echo "<br/>";
            $headfromlog = $gitsetup->getHeadFromGitLog();
//            echo "GILOG  HEAD ";
//            print_r($headfromlog);
//            echo"<br/>";
            $remoterepohead = $gitsetup->getRemoteRepoHead($branchname, $remotereponame);
//            echo "REMNOTE REPO HEAD ";
//            print_r($remoterepohead);
//            echo"<br/>";
//            echo "Head comparison ";
//            print_r((strcmp($headfromlog['commitid'], $remoterepohead['info'])));
            if((strcmp($headfromlog['commitid'], $remoterepohead['info']) == 0) && $temp['status'] == 1 )
            {
//                echo "success " ;
                $flag = true;
                $this->assertTrue(true);
                $this->showResults(true, 'Checking the GitCloudPush function , expected : True ; actual: '.$flag);
                //IMPORTANT : CALL clearSaveRempteRepoTesting to clear up the testing environment
            }
            else {
                $flag = false;
                $this->assertTrue(false);
                $this->showResults(true, 'Checking the GitCloudPush function , expected : True ; actual: '.$flag);
            }
        }else
        {
            $this->assertTrue(false);
        }

    }

    //testing is done for type = commit
    public function ContBackupDb()
    {
        $action = 'contBackupDB';
        $this->setPosttest($action);
        $this->setPostVariablesforTets('table', 'qatest-contbackupdb'); //random table name , its only used as a lable to identiofy which tables is been backed up
        $this->setPostVariablesforTets('key', ' '); //only used for remote backups and hence can be set to null in test cases
        $this->setPostVariablesforTets('type', 'commit'); // imp needs to be set, for test cases we are testing the commit case
        $result = $this->oseFirewall->testController('gitbackupController', $action);
        if(array_key_exists('table', $result) && array_key_exists('type' , $result))
        {
            //has table content
            $sqlfileexists = $this->CheckSqlFileExistsForEachBackedUpTables();
            $dbtablefileexists = file_exists(OSE_DBTABLESFILE);
            $flag = $dbtablefileexists && $sqlfileexists;
            $this->assertTrue($flag);
            $this->showResults(true, 'Checking the ContBackupDb function - The table Backup Part , expected : True ; actual: '.$flag);
        }
        else{
            //return the json message with the status of the operation
            $decoded_result = json_decode($result);
//            print_r($result);
            if(property_exists($decoded_result,'result'))
            {
                $altertablefile = CENTRORABACKUP_FOLDER . ODS . "alterTables.sql";
//                echo "Does db tables file exists : ".file_exists(OSE_DBTABLESFILE);
//                echo"<br/>";
//                echo "Does alter tables file exists : ".file_exists($altertablefile);
//                echo"<br/>";
//                echo "the status of the result is ".$decoded_result->result->status;
                if($decoded_result->result->status == 1 && file_exists(OSE_DBTABLESFILE) == false && file_exists($altertablefile))
                {   $flag =true;
                    $this->assertTrue($flag);
                    $this->showResults(true, 'Checking the ContBackupDb function - The commit part, expected : true ; actual: '.$flag);
                }
                else {
                    //status == 0  problem in committting
                    $this->assertEquals('1', $decoded_result->result->status);
//                    $this->showResults(true, 'Checking the ContBackupDb function - The commit part, expected : 1 ; actual: '.$decoded_result->result->status);
                }

            }else {
                //property result does not exists
                $this->assertTrue(false);
//                $this->showResults(true, 'Checking the ContBackupDb function - The commit part, expected : 1 ; actual: '.$decoded_result->result->status);
            }
        }

    }

    public function LocalBackup()
    {
        $action = 'localBackup';
        $this->setPosttest($action);
        $this->setPostVariablesforTets('type', 'commit');
        $result = $this->oseFirewall->testController('gitbackupController', $action);
//        print_r($result);
        if($result['status'] == 1 && file_exists(FOLDER_LIST))
        {
            $this->assertEquals('1',$result['status']);
            $this->showResults(true, 'Checking the LocalBackup function , expected : 1 ; actual: '.$result['status']);
        }else if($result['status'] == 2){
            $this->assertEquals('2',$result['status']);
            $this->showResults(true, 'Checking the LocalBackup function - No new changes to commit , expected : 2 ; actual: '.$result['status']);
        }
        else {
            $this->assertTrue(false);
            $this->showResults(true, 'Checking the LocalBackup function , expected : 1 ; actual: '.$result['status']);
        }

    }

    public function ContLocalBackup()
    {
        $action = 'contLocalBackup';
        $this->setPosttest($action);
        $this->setPostVariablesforTets('type', 'commit');
        $result = $this->oseFirewall->testController('gitbackupController', $action);
//        print_r($result);
        //if there are no new chnages to commit
        if($result['status'] == 2 && file_exists(FOLDER_LIST) == false ){
            $this->assertEquals('2',$result['status']);
            $this->showResults(true, 'Checking the ContLocalBackup function - No new changes to commit , expected : 2; actual: '.$result['status']);
        }
        else if($result['status'] == 4 && file_exists(FOLDER_LIST) == false ){
                $this->assertEquals('4',$result['status']);
                $this->showResults(true, 'Checking the ContLocalBackup function - rest of the files , expected : 4; actual: '.$result['status']);
            }
        else if($result['status'] == 1 )
        {
            $this->assertEquals('1',$result['status']);
            $this->showResults(true, 'Checking the ContLocalBackup function , expected : 1 ; actual: '.$result['status']);
        }else
        {
            $this->assertTrue(false);
            $this->showResults(true, 'Checking the ContLocalBackup function , expected : 1 ; actual: '.$result['status']);
        }
    }


    public function addblankfile()
    {
        $filepath = OSE_QATESTFILE;
        $result = touch($filepath);
        return $result;
    }

    public function deleteQATestFile()
    {
        if(file_exists(OSE_QATESTFILE))
        {
            unlink(OSE_QATESTFILE);
        }
    }


}
?>